import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddArticleFormComponent } from './add-article-form/add-article-form.component';
import { ArticlesByCategoryComponent } from './articles-by-category/articles-by-category.component';
import { EditArticleFormComponent } from './edit-article-form/edit-article-form.component';
import { HomeComponent } from './home/home.component';
import { SingleArticleComponent } from './single-article/single-article.component';

const routes: Routes = [ 
{path: '', component: HomeComponent}, //Home page
{path: 'add-article', component: AddArticleFormComponent},//add article form
{path:'article/:id', component:SingleArticleComponent},//display one article
{path:'article/edit/:id',component:EditArticleFormComponent},//edit article form
{path:'category/:id',component:ArticlesByCategoryComponent}
]
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
