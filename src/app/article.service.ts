import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Article } from './entities';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ArticleService {
  getAll(){
    return this.http.get<Article[]>(environment.articleUrl);
    
  }
  getByCategory(id:number){
    return this.http.get<Article[]>(environment.articleUrl+"category/"+id);
  }
  getById(id:number){
    return this.http.get<Article>(environment.articleUrl+id);
  }
  add(article:Article){
    return this.http.post<Article>(environment.articleUrl,article);
  }

  update(article:Article){
    return this.http.put<Article>(environment.articleUrl+article.id,article);
  }
  delete(id:number){
    return this.http.delete<number>(environment.articleUrl+id);
  }
  constructor(private http:HttpClient) { }
}
