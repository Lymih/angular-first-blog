import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import {HttpClientModule} from "@angular/common/http";
import { HomeComponent } from './home/home.component';
import { AddArticleFormComponent } from './add-article-form/add-article-form.component';
import { FormsModule } from '@angular/forms';
import { SingleArticleComponent } from './single-article/single-article.component';
import { EditArticleFormComponent } from './edit-article-form/edit-article-form.component';
import { ArticlesByCategoryComponent } from './articles-by-category/articles-by-category.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AddArticleFormComponent,
    SingleArticleComponent,
    EditArticleFormComponent,
    ArticlesByCategoryComponent



  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
