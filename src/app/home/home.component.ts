import { Component, OnInit } from '@angular/core';
import { ArticleService } from '../article.service';
import { Article } from '../entities';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
articles:Article[]=[];
  constructor(private aS:ArticleService) { }

  ngOnInit(): void {
    this.aS.getAll().subscribe(data =>this.articles=data);
  }

}
