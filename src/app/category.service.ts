import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Category } from './entities';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  getAll(){
    return this.http.get<Category[]>(environment.categoryUrl);
    
  }
  getById(id:number){
    return this.http.get<Category>(environment.categoryUrl+id);
  }
  constructor(private http:HttpClient) { }
}
