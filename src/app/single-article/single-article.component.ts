import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ArticleService } from '../article.service';
import { Article } from '../entities';

@Component({
  selector: 'app-single-article',
  templateUrl: './single-article.component.html',
  styleUrls: ['./single-article.component.css']
})
export class SingleArticleComponent implements OnInit {
article:Article={id:1,category:{id:1,label:''},title:'',author:'',content:''}
routeId?:string;

  constructor(private aS:ArticleService, private route:ActivatedRoute, private router:Router) { }
  
  delete(){
    this.aS.delete(this.article.id!).subscribe(()=>this.router.navigateByUrl("/"));
  }
  ngOnInit(): void { 

    this.route.params.subscribe(param => {this.routeId=param['id'];});
    this.aS.getById(Number(this.routeId)).subscribe(data => this.article=data);
  }

}
