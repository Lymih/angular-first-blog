import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ArticleService } from '../article.service';
import { CategoryService } from '../category.service';
import { Article, Category } from '../entities';

@Component({
  selector: 'app-edit-article-form',
  templateUrl: './edit-article-form.component.html',
  styleUrls: ['./edit-article-form.component.css']
})
export class EditArticleFormComponent implements OnInit {
  categories?: Category[];
  article:Article={category:{id:1,label:'Technology'},title:'',author:'',content:''}
  routeId?:string;
  constructor(private aS: ArticleService, private cS:CategoryService, private route:ActivatedRoute, private router:Router) { }
  onSubmit(){
      this.article.date= new Date();
      this.editArticle(this.article);
  }
  editArticle(article:Article){
    console.log(article);
    this.aS.update(article).subscribe((data)=>{
    this.router.navigateByUrl("/")});

  }
  ngOnInit(): void {
    this.route.params.subscribe(param => {this.routeId=param['id'];});
    this.aS.getById(Number(this.routeId)).subscribe(data => this.article=data);
    this.cS.getAll().subscribe(data => this.categories=data);
    console.log(this.article);
    

  }

}
