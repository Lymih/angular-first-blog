import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { ArticleService } from '../article.service';
import { CategoryService } from '../category.service';
import { Article, Category } from '../entities';

@Component({
  selector: 'app-add-article-form',
  templateUrl: './add-article-form.component.html',
  styleUrls: ['./add-article-form.component.css']
})
export class AddArticleFormComponent implements OnInit {

categories:Category[]=[];
article:Article={category:{id:1,label:''},title:'',author:'',content:''}

onSubmit(){
  this.article.date= new Date();
  this.addArticle()
  console.log(this.article);
 
}
addArticle(){
this.aS.add(this.article).subscribe(()=>this.router.navigateByUrl("/"));
}
  constructor(private cS:CategoryService, private aS:ArticleService, private router:Router) { }

  ngOnInit(): void {
    this.cS.getAll().subscribe(data =>this.categories=data);

  }

}
