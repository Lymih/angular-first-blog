import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ArticleService } from '../article.service';
import { CategoryService } from '../category.service';
import { Article, Category } from '../entities';

@Component({
  selector: 'app-articles-by-category',
  templateUrl: './articles-by-category.component.html',
  styleUrls: ['./articles-by-category.component.css']
})
export class ArticlesByCategoryComponent implements OnInit {
  category: Category = { id: 1, label: "" };
  articles: Article[] = [];
  routeId?:String;
  constructor(private aS:ArticleService, private cS:CategoryService, private route:ActivatedRoute) { }

  ngOnInit(): void {
    this.route.params.subscribe(param => {this.routeId=param['id'];})
    this.cS.getById(Number(this.routeId)).subscribe(data=>this.category=data);
    this.aS.getByCategory(Number(this.routeId)).subscribe(data=>this.articles=data);
  }

}
