export interface Category {
id?:number;
label:string;
}

export interface Article {
id?:number;
category:Category;
title:String;
date?:Date;
author:string;
content:string;
}